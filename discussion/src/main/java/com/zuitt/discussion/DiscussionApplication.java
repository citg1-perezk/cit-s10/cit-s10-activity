package com.zuitt.discussion;

import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	// Retrieve all posts
	// localhost:8080/posts
	@RequestMapping(value="/users", method = RequestMethod.GET)
	public String getAllUsers(){
		return "All users retrieved";
	}
	@RequestMapping(value="/users", method = RequestMethod.POST)
	public String createUsers(){
		return "New user created.";
	}

	@RequestMapping(value="/users/{userid}",method=RequestMethod.GET)
	public String getSpecificUsers(@PathVariable Long userid){
		return "User "+userid+" has been retrieved.";
	}

	@RequestMapping (value="/users/{userid}", method = RequestMethod.PUT)
	public User updatePost(@PathVariable Long userid, @RequestBody User user){
		return user;
	}
	/*@RequestMapping(value="/myPosts", method = RequestMethod.GET)
	public String getMyPosts(@RequestHeader(value="Authorization")String user){
		return "Posts for "+ user + " have been retrieved";
	}*/

	@RequestMapping (value="/users/{userid}", method = RequestMethod.DELETE)
	public String deleteUser(@RequestHeader(value = "Authorization")String user,@PathVariable("userid")long userid){
		if(user.isEmpty()){
			return "Unauthorized access";
		}
		else
			return "The user "+userid+" has been deleted";

	}








	@RequestMapping(value="/posts", method= RequestMethod.GET)
	public String getPosts(){
		return "All posts retrieved.";
	}
	@PostMapping("/posts")
	//@RequestMapping (value="/posts", method=RequestMethod.POST)
	public String createPost(){
		return "New post created.";
	}
	//Retrieving a single post
	//localhost:8080/posts/1234
	@GetMapping("posts/{postid}")
	//@RequestMapping (value="/posts/{postid}", method = RequestMethod.GET)
	public String getPost(@PathVariable Long postid){
		return "Viewing details of post "+postid;
	}
	//deleting a post
	//localhost:8080/posts/1234
	@DeleteMapping("posts/{postid}")
	//@RequestMapping (value="/posts/{postid}", method = RequestMethod.DELETE)
	public String deletePost(@PathVariable Long postid){
		return "The post "+postid+ " has been deleted.";
	}
	//Updating a post
	//localhost:8080/posts/1234
	//@RequestMapping (value="/posts/{postid}", method = RequestMethod.PUT)
	//Automtically converts the format to JSON.
	@PutMapping("/posts/{postid}")
	@ResponseBody
	public Post updatePost(@PathVariable Long postid, @RequestBody Post post){
		return post;
	}

	// Retrieving a post for a particular user.
	// localhost:8080/myPosts
	@RequestMapping(value="/myPosts", method = RequestMethod.GET)
	public String getMyPosts(@RequestHeader(value="Authorization")String user){
		return "Posts for "+ user + " have been retrieved";
	}


}
